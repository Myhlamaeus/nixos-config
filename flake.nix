{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";

    flake-parts = {
      url = "github:hercules-ci/flake-parts";
      inputs.nixpkgs-lib.follows = "nixpkgs";
    };

    devenv = {
      url = "github:cachix/devenv";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-hardware.url = "github:NixOS/nixos-hardware";

    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "felschr-nixos/flake-utils";
    };

    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "felschr-nixos/flake-utils";
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };

    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";

    cheatsheets = {
      url = "github:cheat/cheatsheets/master";
      flake = false;
    };

    felschr-nixos = {
      url = "gitlab:FelschR/nixos-config";
      inputs.flake-parts.follows = "flake-parts";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-unstable.follows = "nixpkgs-unstable";
      inputs.home-manager.follows = "home-manager";
      inputs.nixos-hardware.follows = "nixos-hardware";
      inputs.agenix.follows = "agenix";
      inputs.firefox-addons.follows = "firefox-addons";
      inputs.pre-commit-hooks.follows = "devenv/pre-commit-hooks";
      inputs.deploy-rs.follows = "deploy-rs";
    };

    funkwhale = { url = "github:mmai/funkwhale-flake"; };

    gitignore = {
      url = "github:toptal/gitignore/master";
      flake = false;
    };

    veloren.url = "gitlab:veloren/veloren/v0.15.0";
  };

  outputs = { self, nixpkgs, flake-parts, nixpkgs-unstable, ... }@inputs:

    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = [ "x86_64-linux" "aarch64-linux" ];

      imports = [
        inputs.devenv.flakeModule
        inputs.treefmt-nix.flakeModule
        ./flakeModules/nixos
      ];

      nixos = {
        sharedConfiguration.modules =
          [ ./shared { networking.domain = "maurice-dreyer.name"; } ];

        configurations.home-desktop = {
          modules = [{
            imports = (with nixpkgs.nixosModules; [ notDetected ])
              ++ (with inputs.nixos-hardware.nixosModules; [
                common-gpu-nvidia-nonprime
                common-cpu-intel-cpu-only
                common-pc
                common-pc-hdd
                common-pc-ssd
              ]) ++ [
                # Something about this broke in 20.09
                # "${ nixpkgs.outPath }/nixos/modules/profiles/hardened.nix"
                ./shared
                ./desktop/hardware-configuration.nix
                ./personal-computer
                ./desktop
              ];

            nixpkgs.hostPlatform = "x86_64-linux";
            home-manager.users.Myhlamaeus = { custom.games.enable = true; };
          }];
        };

        configurations.rpi = {
          nixpkgs = nixpkgs-unstable;
          modules = [{
            imports = (with nixpkgs.nixosModules; [ notDetected ])
              ++ (with inputs.nixos-hardware.nixosModules; [
                raspberry-pi-4
                common-pc
                common-pc-ssd
              ]) ++ [ ./rpi/hardware-configuration.nix ./server ./rpi ];

            nixpkgs.hostPlatform = "aarch64-linux";
            nixpkgs.config.allowUnfree = true;
          }];
        };

        configurations.sigma = {
          modules = [{
            imports = (with nixpkgs.nixosModules; [ notDetected ])
              ++ (with inputs.nixos-hardware.nixosModules; [
                common-pc
                common-pc-ssd
                common-cpu-intel
              ]) ++ [
                ./shared
                ./sigma/hardware-configuration.nix
                ./server
                ./sigma
              ];

            nixpkgs.hostPlatform = "x86_64-linux";
          }];
        };

        configurations.nixos-iso = {
          modules = [{
            imports = [
              (nixpkgs
                + "/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix")
              ./server
            ];
          }];

          asNixosConfiguration.enable = false;
          asPackage = {
            enable = true;
            configToPackage = cfg: cfg.config.system.build.isoImage;
          };
        };
      };

      flake = {
        flakeModules.nixos = import ./flakeModules/nixos;

        nixosModules.addInputsToRegistry =
          import ./nixosModules/addInputsToRegistry;
        nixosModules.btrfsBackups = import ./nixosModules/btrfsBackups;
        nixosModules.btrfsOptInState = import ./nixosModules/btrfsOptInState;
        nixosModules.fontOverrides = import ./nixosModules/fontOverrides;
        nixosModules.nginxGateway = import ./nixosModules/nginxGateway;
        nixosModules.postgresqlInit = import ./nixosModules/postgresqlInit;

        homeManagerModules.firefoxAutoProfile =
          import ./homeManagerModules/firefoxAutoProfile;
        homeManagerModules.homeDirPermissions =
          import ./homeManagerModules/homeDirPermissions;
        homeManagerModules.tridactyl = import ./homeManagerModules/tridactyl;

        deploy.nodes.rpi = let cfg = self.nixosConfigurations.rpi;
        in {
          hostname = "rpi";
          profiles.system = {
            sshUser = "nixops";
            sshOpts = [ "-t" ];
            user = "root";
            path = inputs.deploy-rs.lib.aarch64-linux.activate.nixos cfg;
            magicRollback = false;
          };
        };

        deploy.nodes.sigma = let cfg = self.nixosConfigurations.sigma;
        in {
          hostname = "sigma";
          profiles.system = {
            sshUser = "nixops";
            sshOpts = [ "-t" ];
            user = "root";
            path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos cfg;
            autoRollback = false;
            magicRollback = false;
          };
        };
      };
      perSystem = { config, pkgs, inputs', self', system, ... }: {
        devenv.shells.default = {
          dotenv.disableHint = true;

          languages = { nix.enable = true; };

          pre-commit = {
            hooks = {
              nixfmt.enable = true;
              fourmolu.enable = true;
            };

            tools = {
              fourmolu = pkgs.lib.mkForce pkgs.haskellPackages.fourmolu;
            };
          };

          scripts.enter-boot-passphrase.exec = let
            exponentialBackoff = pkgs.writeShellScript "exponentialBackoff" ''
              # https://www.deploymastery.com/2023/05/24/how-to-implement-exponential-backoff-in-bash/
              START_DELAY=$1
              MAX_RETRIES=$2
              shift 2

              delay=$START_DELAY
              for i in $(seq 1 "$MAX_RETRIES"); do
                # Try the operation
                if "$@"; then
                  exit 0
                fi

                if [[ $i -lt "$MAX_RETRIES" ]]; then
                  sleep "$delay"
                  delay=$((delay * 2))
                fi
              done
              exit 1
            '';
            enterPassphrase = pkgs.writeShellScript "enterPassphrase" ''
              pass show private/home/computer/"$1"/encryptionKey | ssh -T root@"$1"
            '';
            wrapWithBackoff = { startDelay ? 1, maxRetries ? 5, command }:
              pkgs.writeShellScript "${command.name}-with-backoff" ''
                ${exponentialBackoff} ${toString startDelay} ${
                  toString maxRetries
                } ${command} "$@"
              '';
          in builtins.readFile (wrapWithBackoff {
            maxRetries = 10;
            command = enterPassphrase;
          });

          packages = [
            config.treefmt.build.wrapper
            inputs'.deploy-rs.packages.deploy-rs
          ] ++ (with pkgs; [
            (ghc.withPackages (ps:
              with ps; [
                xmonad
                xmonad-contrib
                containers
                lens
                DescriptiveKeys
              ]))
            haskellPackages.haskell-language-server
            nixfmt
            rage
          ]);
        };

        treefmt = {
          programs = {
            nixfmt.enable = true;
            prettier.enable = true;
            shellcheck.enable = true;
            shfmt.enable = true;
            ormolu = {
              enable = true;
              package = pkgs.haskellPackages.fourmolu;
            };
          };

          projectRootFile = "flake.nix";

          settings.formatter = let
            shell = {
              includes = [ "scripts/*" ];
              excludes = [ "scripts/*.js" ];
            };
          in {
            prettier.excludes = [ "**/snippets/*/*.*" "**/templates/*.*" ];
            shellcheck = shell;
            shfmt = shell;
          };
        };

      };
    };
}
