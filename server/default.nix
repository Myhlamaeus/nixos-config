{ config, pkgs, lib, ... }:

let
  inherit (builtins) readFile;
  inherit (lib) mkIf mkDefault;

in {
  environment.systemPackages = with pkgs; [ git rxvt_unicode.terminfo ];

  services.openssh = {
    enable = true;

    settings = {
      KbdInteractiveAuthentication = false;
      PasswordAuthentication = false;
      PermitRootLogin = mkDefault "no";
    };
  };

  boot.initrd.network.ssh = {
    enable = true;
    authorizedKeys = [ (readFile ../users/Myhlamaeus/ssh.pub) ];
  };

  security.acme = {
    defaults.email = "dreyer.maltem+dev@gmail.com";
    acceptTerms = true;
  };

  services.nginx = {
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;
  };
  users.users.nginx =
    mkIf config.services.nginx.enable { extraGroups = [ "acme" ]; };

  networking.firewall = { enable = true; };

  users.users.Myhlamaeus = {
    isNormalUser = true;
    shell = pkgs.zsh;
    home = "/home/Myhlamaeus";
    extraGroups = [ "wheel" "docker" ];
    passwordFile = config.age.secrets."users/Myhlamaeus/encryptedPassword".path;
    openssh.authorizedKeys.keyFiles = [ ../users/Myhlamaeus/ssh.pub ];
  };
  programs.zsh.enable = true;

  users.users.nixops = {
    isSystemUser = true;
    isNormalUser = false;
    shell = pkgs.runtimeShell;
    group = "nixops";
    openssh.authorizedKeys.keyFiles = [ ../users/Myhlamaeus/ssh.pub ];
  };
  users.groups.nixops = { };
  security.sudo.extraRules = [{
    users = [ "nixops" ];
    commands = [{
      command = "ALL";
      options = [ "NOPASSWD" ];
    }];
  }];

  nix.settings.trusted-users = [ "root" "nixops" ];
}
