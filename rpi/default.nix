{ config, pkgs, lib, ... }:

{
  imports = [
    ./backups.nix
    ./funkwhale.nix
    ./grocy.nix
    ./prometheus.nix
    ./secrets.nix
    ./webdav.nix
  ];

  programs.gnupg.agent = {
    enable = true;
    pinentryFlavor = "curses";
  };

  custom = {
    # backups.enable = true;
    # funkwhale.enable = true;
    grocy.enable = false;
    prometheus.enable = true;
    webdav.enable = true;
  };

  services.nginx = {
    enable = true;

    virtualHosts = {
      ${config.networking.domain} = {
        enableACME = true;
        forceSSL = true;
      };
    };
  };

  networking.firewall = {
    allowedTCPPorts = [ 80 443 3478 5349 ];
    allowedUDPPorts = [ 3478 5349 ];
    allowedUDPPortRanges = [{
      from = 49152;
      to = 65535;
    }];
  };

  networking.wireless.enable = false;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "21.03"; # Did you read the comment?
  home-manager.users.Myhlamaeus.home.stateVersion = "21.05";
}
