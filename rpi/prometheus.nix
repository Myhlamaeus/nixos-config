{ config, pkgs, lib, ... }:

with lib;

let cfg = config.custom.prometheus;

in {
  options.custom.prometheus = {
    enable = mkEnableOption "custom.prometheus";

    serverName = mkOption {
      type = with types; str;
      default = config.networking.domain;
    };

    prometheusPort = mkOption {
      type = with types; port;
      default = 9090;
    };

    prometheusHostname = mkOption {
      type = with types; str;
      default = "prometheus.${cfg.serverName}";
    };
  };

  config = mkIf cfg.enable {
    services.nginx.statusPage = true;

    services.prometheus = {
      enable = true;
      port = cfg.prometheusPort;
      webExternalUrl = "https://${cfg.prometheusHostname}";
      extraFlags =
        [ "--web.config.file=${cfg.age.secrets."prometheus/web.yaml".path}" ];

      exporters = {
        node.enable = true;
        nginx.enable = true;
        postgres.enable = true;
      };

      scrapeConfigs = (map (n: {
        job_name = if builtins.typeOf n == "string" then
          "rpi-${n}"
        else
          "${n.host}-${n.name}";
        static_configs = [{
          targets = [
            (if builtins.typeOf n == "string" then
              "127.0.0.1:${
                toString config.services.prometheus.exporters.${n}.port
              }"
            else
              "${n.host}.maurice-dreyer.name:${toString n.port}")
          ];
        }];
      }) [
        "node"
        "nginx"
        "postgres"
        {
          name = "node";
          port = 9100;
          host = "desk";
        }
      ]) ++ [
        # {
        #   job_name = "rpi-hass";
        #   scrape_interval = "60s";
        #   metrics_path = "/api/prometheus";

        #   # Long-Lived Access Token
        #   authorization = { credentials = "your.longlived.token"; };

        #   scheme = "https";
        #   static_configs = [{ targets = [ "${config.services.homeAssistantHostname}:${config.services.homeAssistantPort}" ]; }];
        # }
      ];
    };

    # services.home-assistant.config.prometheus = { namespace = "hass"; };

    services.nginx.virtualHosts.${cfg.prometheusHostname} = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString cfg.prometheusPort}";
        proxyWebsockets = true;
      };
    };
  };
}
