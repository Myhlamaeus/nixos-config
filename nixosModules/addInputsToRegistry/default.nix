{ config, inputs, lib, ... }:

let
  inherit (lib)
    mkOption mkEnableOption mkIf mkDefault types pipe filterAttrs mapAttrs
    mapAttrsToList;
  cfg = config.nix.inputs;
  inputIsFlake = i: i ? outputs;
in {
  options.nix.inputs = {
    addToRegistry = mkEnableOption "add inputs to registry";
  };

  config.nix = {
    inputs.addToRegistry = mkDefault true;

    registry = mkIf cfg.addToRegistry (pipe inputs [
      (filterAttrs (n: inputIsFlake))
      (mapAttrs (n: i: { flake = i; }))
    ]);
  };
}
