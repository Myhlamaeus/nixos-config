{ config, pkgs, lib, ... }:

let
  inherit (lib) mkOption mkIf types pipe mapAttrs' nameValuePair escapeShellArg;
  cfg = config.services.postgresqlInit;
in {
  options.services.postgresqlInit = {
    users = mkOption {
      type = types.attrsOf (types.submodule ({ name, config, ... }: {
        options = {
          username = mkOption {
            type = types.str;
            default = name;
          };

          systemUsername = mkOption {
            type = types.str;
            default = config.username;
          };

          passwordFile = mkOption { type = types.str; };

          before = mkOption {
            type = types.listOf types.str;
            default = [ ];
          };

          psqlCmd = mkOption {
            type = types.str;
            default = "${pkgs.postgresql}/bin/psql";
          };
        };
      }));
      default = { };
    };
  };

  config = mkIf (cfg.users != { }) {
    systemd.services = pipe cfg.users [
      (mapAttrs' (n:
        { username, systemUsername, passwordFile, before, psqlCmd }:
        nameValuePair "postgresql-init-${n}" {
          enable = true;
          description = "Set up password of ${username}";
          requires = [ "postgresql.service" ];
          after = [ "postgresql.service" ];
          inherit before;
          wantedBy = [ "multi-user.target" ];
          serviceConfig = {
            Type = "oneshot";
            User = systemUsername;
            LoadCredential = [ "password:${passwordFile}" ];
          };
          script = ''
            echo "Set ${username} postgres user password"
            password="$(<"$CREDENTIALS_DIRECTORY/password")"
            ${psqlCmd} \
              -U ${escapeShellArg username} \
              -c "alter user ${username} with password '$password'"
          '';
        }))
    ];
  };
}
