{ config, lib, pkgs, utils, ... }:

let
  inherit (lib)
    mkEnableOption mkOption types pipe mapAttrs' filterAttrs nameValuePair
    optionalAttrs escapeShellArg;
  # Type for a valid systemd unit option. Needed for correctly passing "timerConfig" to "systemd.timers"
  inherit (utils.systemdUtils.unitOptions) unitOption;
  cfg = config.services.btrfsBackups;
  runBackup = pkgs.writeShellApplication {
    name = "btrfsBackups-runBackup";
    runtimeInputs = [ pkgs.btrfs-progs ];
    text = builtins.readFile ./runBackup;
  };

in {
  options.services.btrfsBackups = {
    defaultPostprocessCommand = mkOption {
      type = types.package;
      example = ''
        pkgs.writeScript '''
          ''${pkgs.rage}/bin/rage -r 'age…'
        '''
      '';
    };

    defaultUploadCommand = mkOption {
      type = types.package;
      example = ''
        pkgs.writeScript '''
          volume=$1
          time=$2
          ''${pkgs.rclone}/bin/rclone rcat ":b2:''${bucket}/''${config.networking.domain}/''${config.networking.hostName}/$vol/$time.age"
        '''
      '';
    };

    backups = mkOption {
      description = lib.mdDoc ''
        Periodic backups to create with Restic.
      '';
      type = types.attrsOf (types.submodule ({ config, name, ... }: {
        options = {
          enable = mkEnableOption "btrfsBackups ${name}";

          postprocessCommand = mkOption {
            type = types.package;
            default = cfg.defaultPostprocessCommand;
            example = ''
              pkgs.writeScript '''
                ''${pkgs.rage}/bin/rage -r 'age…'
              '''
            '';
          };

          uploadCommand = mkOption {
            type = types.package;
            default = cfg.defaultUploadCommand;
            example = ''
              pkgs.writeScript '''
                bak=$1
                volume=$2
                time=$3
                ''${pkgs.rclone}/bin/rclone copyto "$bak" ":b2:bucket/''${config.networking.domain}/''${config.networking.hostName}/$volume/$time"
              '''
            '';
          };

          environmentFile = mkOption {
            type = with types; nullOr str;
            default = null;
            description = lib.mdDoc ''
              file containing the credentials to access the repository, in the
              format of an EnvironmentFile as described by systemd.exec(5)
            '';
          };

          mountPoint = mkOption { type = types.path; };

          subvolumes = mkOption {
            type = types.listOf types.str;
            default = [ name ];
            example = [ "home" "persist" ];
          };

          backupPath = mkOption {
            type = types.str;
            default = ".backups";
          };

          timerConfig = mkOption {
            type = types.attrsOf unitOption;
            default = {
              OnCalendar = "hourly";
              Persistent = true;
            };
            description = lib.mdDoc ''
              When to run the backup. See {manpage}`systemd.timer(5)` for details.
            '';
            example = {
              OnCalendar = "00:05";
              RandomizedDelaySec = "5h";
              Persistent = true;
            };
          };

          user = mkOption {
            type = types.str;
            default = "root";
            description = lib.mdDoc ''
              As which user the backup should run.
            '';
            example = "postgresql";
          };

          backupPrepareCommand = mkOption {
            type = with types; nullOr str;
            default = null;
            description = lib.mdDoc ''
              A script that must run before starting the backup process.
            '';
          };

          backupCleanupCommand = mkOption {
            type = with types; nullOr str;
            default = null;
            description = lib.mdDoc ''
              A script that must run after finishing the backup process.
            '';
          };
        };
      }));
      default = { };
      example = {
        localbackup = {
          paths = [ "/home" ];
          exclude = [ "/home/*/.cache" ];
          repository = "/mnt/backup-hdd";
          passwordFile = "/etc/nixos/secrets/restic-password";
          initialize = true;
        };
        remotebackup = {
          paths = [ "/home" ];
          repository = "sftp:backup@host:/backups/home";
          passwordFile = "/etc/nixos/secrets/restic-password";
          extraOptions = [
            "sftp.command='ssh backup@host -i /etc/nixos/secrets/backup-private-key -s sftp'"
          ];
          timerConfig = {
            OnCalendar = "00:05";
            RandomizedDelaySec = "5h";
          };
        };
      };
    };
  };

  config = {
    systemd.services = pipe cfg.backups [
      (filterAttrs (name: backup: backup.enable))
      (mapAttrs' (name: backup:
        nameValuePair "btrfs-backups-${name}" ({
          environment = {
            BTRFS_POSTPROCESS_CMD = backup.postprocessCommand;
            BTRFS_UPLOAD_CMD = backup.uploadCommand;
            BTRFS_MOUNT_POINT = toString backup.mountPoint;
            BTRFS_BACKUP_PATH = backup.backupPath;
          };
          path = [ ];
          restartIfChanged = false;
          serviceConfig = {
            Type = "oneshot";
            ExecStart = map (vol:
              "${runBackup}/bin/btrfsBackups-runBackup ${escapeShellArg vol}")
              backup.subvolumes;
            User = backup.user;
            RuntimeDirectory = "btrfs-backups-${name}";
            CacheDirectory = "btrfs-backups-${name}";
            CacheDirectoryMode = "0700";
            PrivateTmp = true;
          } // optionalAttrs (backup.environmentFile != null) {
            EnvironmentFile = backup.environmentFile;
          };
        } // optionalAttrs (backup.backupPrepareCommand != null) {
          preStart =
            pkgs.writeScript "backupPrepareCommand" backup.backupPrepareCommand;
        } // optionalAttrs (backup.backupCleanupCommand != null) {
          postStop =
            pkgs.writeScript "backupCleanupCommand" backup.backupCleanupCommand;
        })))
    ];
    systemd.timers = pipe cfg.backups [
      (filterAttrs (name: backup: backup.enable))
      (mapAttrs' (name: backup:
        nameValuePair "btrfs-backups-${name}" {
          wantedBy = [ "timers.target" ];
          timerConfig = backup.timerConfig;
        }))
    ];
  };
}
