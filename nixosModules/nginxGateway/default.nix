{ options, config, lib, ... }:

let
  inherit (lib)
    mkEnableOption mkOption types mkIf mkMerge mkDefault pipe mapAttrs'
    nameValuePair;
  cfg = config.services.nginx.gateway;
in {
  options.services.nginx.gateway = mkOption {
    type = types.submodule (mod: {
      options = {
        enable = mkEnableOption "nginx gateway";

        root = mkOption { type = types.str; };

        targetHost = mkOption {
          type = types.str;
          default = "127.0.0.1";
        };

        sharedVhostOptions = mkOption {
          type = options.services.nginx.virtualHosts.type.nestedTypes.elemType;
          default = {
            enableACME = true;
            forceSSL = true;
          };
        };

        services = mkOption {
          type = types.attrsOf (types.submodule ({ name, ... }: {
            options = {
              hostName = mkOption {
                type = types.str;
                default = if name == "" then
                  mod.config.root
                else
                  "${name}.${mod.config.root}";
              };

              targetPort = mkOption { type = types.port; };

              targetHost = mkOption {
                type = types.str;
                default = mod.config.targetHost;
              };

              vhostOptions = mkOption {
                type = types.attrs;
                default = { };
              };
            };
          }));
          default = { };
        };
      };
    });
    default = { };
  };

  config = mkIf cfg.enable {
    services.nginx.virtualHosts = pipe cfg.services [
      (mapAttrs' (n:
        { hostName, targetPort, targetHost, vhostOptions }:
        nameValuePair hostName (mkMerge [
          {
            locations."/" = {
              proxyPass = "http://${targetHost}:${toString targetPort}";
              proxyWebsockets = mkDefault true;
              recommendedProxySettings = mkDefault true;
            };
          }
          cfg.sharedVhostOptions
          vhostOptions
        ])))
      (vhosts: { "${cfg.root}" = cfg.sharedVhostOptions; } // vhosts)
    ];
  };
}
