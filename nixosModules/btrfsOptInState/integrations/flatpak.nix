{ config, pkgs, lib, ... }:

let inherit (lib) mkIf;
in {
  btrfsOptInState.paths =
    mkIf config.services.flatpak.enable [ /var/lib/flatpak ];
}
