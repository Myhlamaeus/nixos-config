{ config, pkgs, lib, ... }:

let
  inherit (lib)
    mkIf mkOption mkMerge types pipe filter mapAttrsToList hasPrefix;
  cfg = config.btrfsOptInState.xdg;

in {
  options.btrfsOptInState = {
    xdg = {
      configPaths = mkOption {
        type = with types; listOf path;
        default = [ ];
      };

      dataPaths = mkOption {
        type = with types; listOf path;
        default = [ ];
      };
    };
  };

  config = mkIf config.xdg.enable {
    btrfsOptInState.paths = mkMerge [
      (map (p: config.xdg.configHome + "${toString p}") cfg.configPaths)
      (map (p: config.xdg.dataHome + "${toString p}") cfg.dataPaths)
      (mkIf config.xdg.userDirs.enable (pipe [
        "desktop"
        "documents"
        "download"
        "music"
        "pictures"
        "publicShare"
        "templates"
        "videos"
      ] [
        (map (n: config.xdg.userDirs.${n}))
        (filter (v: v != null))
        (filter (v: !hasPrefix "/nix/store/" v))
      ]))
      (mkIf config.xdg.userDirs.enable
        (mapAttrsToList (n: v: v) config.xdg.userDirs.extraConfig))
    ];
  };
}
