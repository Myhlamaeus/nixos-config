{ config, pkgs, lib, ... }:

let
  inherit (lib) mkIf mkOption types pipe mapAttrsToList flatten;
  cfg = config.btrfsOptInState;

in {
  options.btrfsOptInState = {
    paths = mkOption {
      type = with types; listOf path;
      default = [ ];
    };

    home.paths = mkOption {
      type = with types; listOf path;
      default = [ ];
    };
  };

  config = {
    btrfsOptInState.paths =
      map (p: config.home.homeDirectory + "${toString p}") cfg.home.paths;
  };
}
