{ config, pkgs, lib, ... }:

let
  inherit (lib) mkIf mkEnableOption pipe mapAttrsToList flatten;
  cfg = config.btrfsOptInState.integrations.home-manager;

in {
  options.btrfsOptInState.integrations = {
    home-manager.enable = mkEnableOption "btrfsOptInState home-manager";
  };

  config = mkIf cfg.enable {
    home-manager.sharedModules = [ ./module.nix ./xdg.nix ];

    btrfsOptInState.paths = pipe config.home-manager.users [
      (mapAttrsToList (n: v: v.btrfsOptInState.paths))
      flatten
    ];
  };
}
