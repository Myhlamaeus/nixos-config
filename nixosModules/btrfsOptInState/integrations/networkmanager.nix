{ config, pkgs, lib, ... }:

let inherit (lib) mkIf;
in {
  btrfsOptInState.paths = mkIf config.networking.networkmanager.enable [
    /etc/NetworkManager/system-connections
    /var/lib/NetworkManager/secret_key
    /var/lib/NetworkManager/seen-bssids
    /var/lib/NetworkManager/timestamps
  ];
}
