{ config, pkgs, lib, ... }:

let inherit (lib) mkIf;
in { btrfsOptInState.paths = [ /etc/adjtime /etc/NIXOS /etc/machine-id ]; }
