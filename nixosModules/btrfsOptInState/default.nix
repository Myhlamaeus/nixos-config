{ config, pkgs, lib, ... }:

let
  inherit (lib)
    types mkOption mkEnableOption mkIf mkBefore pipe nameValuePair listToAttrs
    filterAttrs hasPrefix removePrefix mapAttrs mapAttrs' mapAttrsToList
    escapeShellArg;
  cfg = config.btrfsOptInState;
  paths = pipe cfg.paths [
    (map toString)
    (map (n: nameValuePair n (cfg.target.mountPoint + "${toString n}")))
    listToAttrs
  ];
  diff = pkgs.writeShellApplication {
    name = "opt-in-state-diff";
    runtimeInputs = [ pkgs.btrfs-progs ];
    text = ''
      mnt=${escapeShellArg (toString cfg.btrfs.mountPoint)}
      volume=${escapeShellArg cfg.source.volume}
      emptySnapshot=${escapeShellArg cfg.source.emptySnapshot}

      OLD_TRANSID=$(btrfs subvolume find-new "$mnt/$emptySnapshot" 9999999)
      OLD_TRANSID=''${OLD_TRANSID#transid marker was }

      btrfs subvolume find-new "$mnt/$volume" "$OLD_TRANSID" |
        sed '$d' |
        cut -f17- -d' ' |
        sort |
        uniq |
        while read -r path; do
          path="/$path"
          if [ -L "$path" ]; then
            : # The path is a symbolic link, so is probably handled by NixOS already
          elif [ -d "$path" ]; then
            : # The path is a directory, ignore
          else
            echo "$path"
          fi
        done
    '';
  };
  rollback = pkgs.writeShellApplication {
    name = "opt-in-state-rollback";
    runtimeInputs = [ pkgs.btrfs-progs ];
    text = ''
      device=${escapeShellArg cfg.btrfs.device}
      volume=${escapeShellArg cfg.source.volume}
      emptySnapshot=${escapeShellArg cfg.source.emptySnapshot}

      mkdir -p /mnt

      # We first mount the btrfs root to /mnt
      # so we can manipulate btrfs subvolumes.
      mount -o subvol=/ "$device" /mnt

      # While we're tempted to just delete /root and create
      # a new snapshot from /root-blank, /root is already
      # populated at this point with a number of subvolumes,
      # which makes `btrfs subvolume delete` fail.
      # So, we remove them first.
      #
      # /root contains subvolumes:
      # - /root/var/lib/portables
      # - /root/var/lib/machines
      #
      # I suspect these are related to systemd-nspawn, but
      # since I don't use it I'm not 100% sure.
      # Anyhow, deleting these subvolumes hasn't resulted
      # in any issues so far, except for fairly
      # benign-looking errors from systemd-tmpfiles.
      btrfs subvolume list -o /mnt/"$volume" |
        cut -f9 -d' ' |
        while read subvolume; do
          echo "deleting /$subvolume subvolume..."
          btrfs subvolume delete "/mnt/$subvolume"
        done &&
        echo "deleting /root subvolume..." &&
        btrfs subvolume delete /mnt/root

      echo "restoring blank /$volume subvolume..."
      btrfs subvolume snapshot /mnt/"$emptySnapshot" /mnt/"$volume"

      # Once we're done rolling back to a blank snapshot,
      # we can unmount /mnt and continue on the boot process.
      umount /mnt
    '';
  };

in {
  # meta = {
  #   description =
  #     "BTRFS Opt-In State as per https://mt-caret.github.io/blog/posts/2020-06-29-optin-state.html";
  # };

  imports = [ ./integrations ];

  options.btrfsOptInState = {
    enable = mkEnableOption "btrfsOptInState";
    enableRollback = mkEnableOption "btrfsOptInState rollback";

    btrfs = {
      device = mkOption { type = with types; str; };

      mountPoint = mkOption {
        type = with types; path;
        default = /btrfs;
      };
    };

    source = {
      volume = mkOption {
        type = with types; str;
        default = "root";
      };

      emptySnapshot = mkOption {
        type = with types; str;
        default = "root-blank";
      };
    };

    target = {
      volume = mkOption {
        type = with types; str;
        default = "persist";
      };

      mountPoint = mkOption {
        type = with types; path;
        default = /persist;
      };
    };

    paths = mkOption {
      type = with types; listOf path;
      default = [ ];
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ diff ];

    environment.etc = pipe paths [
      (filterAttrs (n: v: hasPrefix "/etc/" n))
      (mapAttrs' (n: nameValuePair (removePrefix "/etc/" n)))
      (mapAttrs (n: v: { source = toString v; }))
    ];

    systemd.tmpfiles.rules = pipe paths [
      (filterAttrs (n: v: !hasPrefix "/etc/" n))
      (mapAttrsToList (n: v: "L ${n} - - - - ${toString v}"))
    ];

    security.sudo.extraConfig = ''
      # rollback results in sudo lectures after each reboot
      Defaults lecture = never
    '';

    fileSystems."${toString cfg.btrfs.mountPoint}" = {
      inherit (cfg.btrfs) device;
      fsType = "btrfs";
      options = [ "subvol=/" ];
    };

    fileSystems."${toString cfg.target.mountPoint}" = {
      inherit (cfg.btrfs) device;
      fsType = "btrfs";
      options = [ "subvol=/${cfg.target.volume}" ];
    };

    boot.initrd.postDeviceCommands = mkIf cfg.enableRollback (mkBefore ''
      ${rollback}/bin/rollback
    '');
  };
}
