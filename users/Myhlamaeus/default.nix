{ config, pkgs, ... }:

{
  security.pam.enableEcryptfs = true;

  users.users.Myhlamaeus = {
    isNormalUser = true;
    shell = pkgs.zsh;
    home = "/home/Myhlamaeus";
    extraGroups = [ "wheel" "docker" "podman" "audio" "jackaudio" ];
    passwordFile = "/etc/nixos/secrets/Myhlamaeus-password";
    openssh.authorizedKeys.keyFiles = [ ./ssh.pub ];
  };

  systemd.tmpfiles.rules = let link = f: t: "L ${f} - - - - ${t}";
  in [
    # per user (move out at some point)
    (link "/home/Myhlamaeus/.config/spacemacs"
      "/etc/nixos/users/Myhlamaeus/spacemacs")
  ];

  home-manager.users.Myhlamaeus = import ./home;
}
