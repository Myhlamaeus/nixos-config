{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}

import Control.Arrow ((&&&), (***))
import Control.Lens (Lens', lens, (&), (.~), (?~))
import Control.Monad (join, (<=<))
import Data.Bifunctor (first)
import Data.Foldable (toList, traverse_)
import Data.List (isInfixOf, sort)
import Data.Map (fromList)
import Data.Map qualified as M
import Data.Text (Text)
import Data.Text qualified as T
import Data.Word (Word32)
import Foreign.C.Types (CInt)
import System.IO (hPutStrLn)
import XMonad hiding (float)
import XMonad.Actions.CopyWindow
import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicWorkspaces (addHiddenWorkspace)
import XMonad.Actions.GridSelect (GSConfig, gridselect, runSelectedAction)
import XMonad.Actions.Minimize (maximizeWindow, minimizeWindow, withMinimized)
import XMonad.Actions.RandomBackground (RandomColor (HSV), randomBg)
import XMonad.Actions.Submap (submap)
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.Accordion (Accordion (Accordion))
import XMonad.Layout.AutoMaster (autoMaster)
import XMonad.Layout.BoringWindows (boringWindows, clearBoring, focusDown, focusMaster, focusUp, markBoring)
import XMonad.Layout.Circle (Circle (Circle))
import XMonad.Layout.DragPane (DragType (Vertical), dragPane)
import XMonad.Layout.Dwindle (Chirality (..), Dwindle (Spiral))
import XMonad.Layout.Dwindle qualified as Dwindle
import XMonad.Layout.Fullscreen (fullscreenFocus)
import XMonad.Layout.Grid (Grid (Grid))
import XMonad.Layout.Groups (group)
import XMonad.Layout.Minimize (minimize)
import XMonad.ManageHook
import XMonad.Prompt (XPConfig, promptKeymap, vimLikeXPKeymap)
import XMonad.Prompt.Layout (layoutPrompt)
import XMonad.StackSet (RationalRect (RationalRect), float, sink)
import XMonad.StackSet qualified as W
import XMonad.Util.EZConfig
import XMonad.Util.NamedActions
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run (spawnPipe)

-- Color of current window title in xmobar.
xmobarTitleColor = "#FFB6B0"

-- Color of current workspace in xmobar.
xmobarCurrentWorkspaceColor = "#CEFFAC"

{- | The available layouts.  Note that each layout is separated by |||, which
 denotes layout choice.
-}
layout = minimize . boringWindows . fullscreenFocus . avoidStruts $ tiled ||| autoMaster 1 (1 / 100) Grid ||| group Full Grid ||| dragPane Vertical 0.1 0.5 ||| Grid ||| Spiral R Dwindle.CW 1.5 1.1 ||| Accordion ||| Circle ||| Full
  where
    -- default tiling algorithm partitions the screen into two panes
    tiled = Tall nmaster delta ratio

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio = 1 / 2

    -- Percent of screen to increment by when resizing panes
    delta = 3 / 100

newtype Workspace
    = WN Int
    deriving (Eq)
wNamedCount = 2

workspaceName :: Workspace -> String
workspaceName (WN n) = show $ n + 1

instance Enum Workspace where
    toEnum = WN

    fromEnum (WN n) = n

instance Bounded Workspace where
    minBound = toEnum 0
    maxBound = toEnum 10

instance Ord Workspace where
    a <= b = fromEnum a <= fromEnum b

workspaces' :: [Workspace]
workspaces' = sort [minBound .. maxBound]

doShift' = doShift . workspaceName

xpConfig :: XPConfig
xpConfig =
    def
        { promptKeymap = vimLikeXPKeymap
        }

gtkSpawn :: MonadIO m => FilePath -> [Text] -> m ()
gtkSpawn name args = spawn $ T.unpack $ T.intercalate " " $ ["__gtk-launch__", T.pack name] <> args

data FirefoxProfile = FPPrivate | FPWork

data App
    = ADesktop FilePath [Text]
    | ACalibre
    | AElement
    | AEmacs (Maybe FilePath)
    | AFirefox FirefoxProfile
    | AKrita
    | ASteam
    | ATelegram
    | AZotero

spawnApp = uncurry gtkSpawn . appToDesktop
  where
    appToDesktop = \case
        ADesktop p args -> (p, args)
        ACalibre -> ("calibre.desktop", [])
        AElement -> ("element-desktop.desktop", [])
        AEmacs p -> ("emacs.desktop", T.pack <$> toList p)
        AFirefox FPPrivate -> ("firefox.desktop", [])
        AFirefox FPWork -> ("firefox-profile-work.desktop", [])
        AKrita -> ("org.kde.krita.desktop", [])
        ASteam -> ("steam.desktop", [])
        ATelegram -> ("org.telegram.desktop", [])
        AZotero -> ("zotero.desktop", [])

_projectName :: Lens' Project String
_projectName = lens projectName (\p h -> p{projectName = h})

_projectDirectory :: Lens' Project String
_projectDirectory = lens projectDirectory (\p h -> p{projectDirectory = h})

_projectStartHook :: Lens' Project (Maybe (X ()))
_projectStartHook = lens projectStartHook (\p h -> p{projectStartHook = h})

proj n d =
    Project
        { projectName = n
        , projectDirectory = d
        , projectStartHook = Nothing
        }

devProject n d =
    proj n d
        & _projectStartHook ?~ spawnApp (AEmacs $ Just ".")

ghqProject platform owner n =
    devProject n ("~/.ghq/" <> platform <> "/" <> owner <> "/" <> n)
gitlabProject = ghqProject "gitlab.com"

orgRoamProject n i =
    proj n "~/org/roam"
        & _projectStartHook ?~ spawnApp (AEmacs $ Just i)

projectGame =
    proj "game" "~"
        & _projectStartHook ?~ spawnApp ASteam

projectReading =
    proj "reading" "~"
        & _projectStartHook ?~ traverse_ spawnApp [ACalibre, AZotero, AEmacs $ Just "/home/Myhlamaeus/media/keybase/private/myhlamaeus/org/roam/20210705145442-reading_list.org"]

projectSocial =
    proj "social" "~"
        & _projectStartHook ?~ traverse_ spawnApp [AElement, ATelegram]

data ProjectTree
    = PTLeaf Project
    | PTNode ProjectName [ProjectTree]

projects :: [ProjectTree]
projects =
    [ PTLeaf $ devProject "org" "~/org"
    , PTLeaf $ proj "listening" "~"
    , PTNode
        "dev"
        [ PTLeaf $ devProject "nixos" "/etc/nixos"
        , PTLeaf $ devProject "dev" "~/.ghq"
        ]
    , PTNode
        "private"
        [ PTLeaf $ proj "health" "~"
        , PTLeaf projectSocial
        , PTLeaf $ proj "home" "~"
        , PTLeaf $ proj "shopping" "~"
        , PTLeaf $ proj "private" "~"
        ]
    , PTLeaf $ proj "watching" "~"
    , PTNode
        "hobby"
        [ PTLeaf projectReading
        , PTLeaf projectGame
        , PTLeaf $
            gitlabProject "Myhlamaeus" "paintings"
                & _projectName .~ "drawing"
                & _projectStartHook ?~ traverse_ spawnApp [AKrita, AEmacs $ Just "index.org"]
        , PTLeaf $ orgRoamProject "cooking" "20220331100510-cooking_recipes.org"
        , PTLeaf $ orgRoamProject "crochet" "20210630124253-crochet_patterns.org"
        , PTLeaf $ orgRoamProject "writing" "20211109100021-writing.org"
        ]
    ]

toPureProjects :: [ProjectTree] -> [Project]
toPureProjects = (fold =<<)
  where
    fold :: ProjectTree -> [Project]
    fold (PTLeaf pr) = [pr]
    fold (PTNode _ pogs) =
        toPureProjects pogs

runSelectedProjectAction :: GSConfig (X ()) -> (Project -> X ()) -> [ProjectTree] -> X ()
runSelectedProjectAction cfg act ps = runSelectedAction cfg $ foldAct <$> ps
  where
    foldAct :: ProjectTree -> (ProjectName, X ())
    foldAct (PTLeaf pr) = (projectName pr, act pr)
    foldAct (PTNode s pogs) = (s, runSelectedProjectAction cfg act pogs)

runSelectedPredefProjectAction :: GSConfig (X ()) -> (Project -> X ()) -> X ()
runSelectedPredefProjectAction cfg act = runSelectedProjectAction cfg act projects

data RofiMode = RMRaw Text | RMRunBin | RMRunDesktop | RMGoToWindow

rofi :: MonadIO m => RofiMode -> m ()
rofi (RMRaw mode) = spawn $ T.unpack $ T.intercalate " " ["__rofi__", "-modi", mode, "-show", mode]
rofi RMRunBin = rofi $ RMRaw "run"
rofi RMRunDesktop = rofi $ RMRaw "drun"
rofi RMGoToWindow = rofi $ RMRaw "window"

class ToNameAndAction a where
    toAction :: a -> X ()
    toName :: a -> String

instance ToNameAndAction RofiMode where
    toAction = rofi
    toName (RMRaw mode) = "rofi " <> T.unpack mode
    toName RMRunBin = "rofi bin"
    toName RMRunDesktop = "rofi desktop"
    toName RMGoToWindow = "rofi go to window"

toNamedAction :: ToNameAndAction a => a -> NamedAction
toNamedAction a = addName (toName a) (toAction a)

additionalKeys' :: XConfig a -> (XConfig a -> [((KeyMask, KeySym), X ())]) -> XConfig a
cfg `additionalKeys'` map = cfg `additionalKeys` (first (first (modMask cfg .|.)) <$> map cfg)

gridselectWindow :: GSConfig Window -> [Window] -> X (Maybe Window)
gridselectWindow cfg = gridselect cfg <=< windowMap
  where
    windowMap = traverse keyValuePair
    keyValuePair w = (,w) <$> windowName w
    windowName = fmap show . getName

shiftToProject' :: Project -> ManageHook
shiftToProject' p = do
    liftX $ addHiddenWorkspace $ projectName p
    doF . W.shiftWin (projectName p) =<< ask

data Lock = Lock | Hibernate

lockX11 :: MonadIO m => m ()
lockX11 = spawn "__xautolock__ -locknow"

hibernate :: MonadIO m => m ()
hibernate = spawn "systemctl hibernate"

instance ToNameAndAction Lock where
    toAction Lock = lockX11
    toAction Hibernate = hibernate
    toName Lock = "lock"
    toName Hibernate = "hibernate"

data KnownService = KSRedshift

instance Show KnownService where
    show KSRedshift = "redshift"

startService, stopService :: MonadIO m => KnownService -> m ()
startService s = spawn $ "systemctl --user start " <> show s
stopService s = spawn $ "systemctl --user stop " <> show s

keymap :: XConfig l -> [((KeyMask, KeySym), NamedAction)]
keymap c =
    [
        ( (hyperMask, xK_p)
        , spawn @X "sh -c '__xsel__ | __xvkbd__ -xsendevent -file -'"
        )
    ]
        ^++^ mkNamedKeymap
            c
            [ ("M-S-<Enter>", addName "shell" $ randomBg $ HSV 40 10)
            , ("M-C-l", toNamedAction Lock)
            , ("M-C-S-l", toNamedAction Hibernate)
            , ("M-C-r", addName "start redshift" $ startService KSRedshift)
            , ("M-C-S-r", addName "stop redshift" $ stopService KSRedshift)
            , ("M-p", toNamedAction RMRunBin)
            , ("M-S-p", toNamedAction RMGoToWindow)
            , ("M-a", toNamedAction RMRunDesktop)
            , ("M-S-a", toNamedAction RMGoToWindow)
            , ("M-v", addName "copy to all" $ windows copyToAll)
            , ("M-S-v", addName "kill all other copies" killAllOtherCopies)
            , ("M-n", addName "PiP" $ withFocused pictureInPicture)
            , ("M-S-n", addName "stop PiP" $ withFocused stopPictureInPicture)
            , ("M-/", addName "switch project" $ runSelectedPredefProjectAction def switchProject)
            , ("M-S-/", addName "shift to project" $ runSelectedPredefProjectAction def shiftToProject)
            , ("M--", addName "minimize" $ withFocused minimizeWindow)
            , ("M-S--", addName "unminimize" $ withMinimized $ maybe (pure ()) maximizeWindow <=< gridselectWindow def)
            , ("M-j", addName "focus down" focusDown)
            , ("M-k", addName "focus up" focusUp)
            , ("M-m", addName "focus first" focusMaster)
            , ("M-x", addName "switch layout" $ layoutPrompt xpConfig)
            ]
  where
    hyperMask = controlMask .|. shiftMask .|. mod1Mask .|. mod4Mask
    pictureInPicture :: Window -> X ()
    pictureInPicture win = whenX (isClient win) $ do
        r <- withDisplay $ \d -> do
            let (dw, dh) = join (***) fromIntegral (displayWidth d 0, displayHeight d 0)
            let (ww, wh) = (500, ceiling @Double $ 500 / 16 * 9) :: (Dimension, Dimension)
            pure $ RationalRect (fromIntegral $ dw - ww - 30) (fromIntegral $ dh - wh - 30) (fromIntegral ww) (fromIntegral wh)
        windows $ float win r
        windows copyToAll
        markBoring
    stopPictureInPicture :: Window -> X ()
    stopPictureInPicture win = whenX (isClient win) $ do
        windows $ sink win
        killAllOtherCopies
        clearBoring

main :: IO ()
main = do
    xmobar <- spawnPipe "__xmobar-with-config__"
    xmonad
        . withUrgencyHook NoUrgencyHook
        . ewmh
        . docks
        . dynamicProjects (toPureProjects projects)
        . addDescrKeys ((mod4Mask .|. shiftMask, xK_h), xMessage) keymap
        $ def
            { normalBorderColor = "#999999"
            , focusedBorderColor = "#cc0000"
            , modMask = mod4Mask -- Use Super instead of Alt
            , terminal = "urxvt"
            , focusFollowsMouse = False
            , layoutHook = layout
            , handleEventHook = handleEventHook def <+> fullscreenEventHook
            , logHook =
                dynamicLogWithPP $
                    xmobarPP
                        { ppOutput = hPutStrLn xmobar
                        , ppTitle = xmobarColor xmobarTitleColor "" . shorten 70
                        , ppCurrent = xmobarColor xmobarCurrentWorkspaceColor ""
                        , ppSep = "   "
                        }
            , workspaces = fmap workspaceName workspaces'
            , manageHook =
                composeAll
                    [ className =? "keybase" --> shiftToProject' projectSocial
                    , className =? "Steam" --> shiftToProject' projectGame
                    , isInfixOf "openmw" <$> className --> shiftToProject' projectGame
                    , className =? "Dwarf_Fortress" --> shiftToProject' projectGame
                    , className =? "dwarftherapist" --> shiftToProject' projectGame
                    , title =? "dfhack" --> shiftToProject' projectGame
                    , isInfixOf "calibre" <$> className --> shiftToProject' projectReading
                    , isInfixOf "goodreads.com" <$> title --> shiftToProject' projectReading
                    , className =? "Xmessage" --> doFloat
                    ]
                    <+> composeOne
                        [ transience
                        , isFullscreen -?> doFullFloat
                        ]
                    <+> def
            }
