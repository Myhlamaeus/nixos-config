{ pkgs, ... }:

let
  inherit (pkgs) formats;
  socketPath = "%t/podman/podman.sock";
  podmanPkg = pkgs.podman;

in {
  xdg.configFile."containers/containers.conf".source =
    (formats.toml { }).generate "containers.conf" {
      engine = {
        init_path = pkgs.catatonit + "/bin/catatonit";
        runtime = "crun";
      };
    };

  systemd.user.sockets.podman = {
    Unit = {
      Description = "Podman API Socket";
      Documentation = "man:podman-system-service(1)";
    };

    Socket = {
      ListenStream = socketPath;
      SocketMode = "0660";
    };

    Install = { WantedBy = [ "sockets.target" ]; };
  };

  systemd.user.services.podman = {
    Unit = {
      Description = "Podman API Service";
      Requires = "podman.socket";
      After = "podman.socket";
      Documentation = "man:podman-system-service(1)";
      StartLimitIntervalSec = 0;
    };

    Service = {
      Type = "exec";
      KillMode = "process";
      Environment = ''LOGGING="--log-level=info"'';
      ExecStart = "${podmanPkg}/bin/podman $LOGGING system service --time=0";
    };

    Install = { WantedBy = [ "default.target" ]; };
  };
}
