{ pkgs, lib, ... }:

let inherit (lib) mkMerge mkAfter mkDefault;
in {
  accounts.email.accounts = {
    home = {
      primary = true;
      imap = {
        host = "127.0.0.1";
        port = 1143;
        tls = {
          enable = true;
          useStartTls = true;
          certificatesFile = "/home/Myhlamaeus/.config/email/cert.pem";
        };
      };
      smtp = {
        host = "127.0.0.1";
        port = 1025;
        tls = {
          enable = true;
          useStartTls = true;
          certificatesFile = "/home/Myhlamaeus/.config/email/cert.pem";
        };
      };
      address = "me@maublew.name";
      userName = "me@maublew.name";
      realName = "Maurice B. Lewis";
      notmuch.enable = true;
      mbsync = {
        enable = true;
        create = "both";
        expunge = "both";
        extraConfig.account.Timeout = 120;
      };
      msmtp = {
        enable = true;
        extraConfig = { auth = "plain"; };
      };
      passwordCommand = "${pkgs.coreutils}/bin/cat ~/.config/email/home";
    };
  };
  programs.notmuch = {
    enable = true;
    maildir.synchronizeFlags = true;
    # hooks.preNew = ''
    #   ${pkgs.afew}/bin/afew -v --move-mails
    # '';
    hooks.postNew = mkMerge [
      ''
        notmuch tag +home -- tag:new and folder:'/^home\//'
        notmuch tag +sent -- tag:new and folder:'/\/Sent$/'
        notmuch tag +dmz -- tag:new and folder:'/\/DMZ$/'
        notmuch tag +killed -- tag:new and folder:'/\/Trash$/'
        notmuch tag +spam -- tag:new and folder:'/\/Spam$/'
        notmuch tag +toMigrate -- tag:new and to:dreyer.maltem@gmail.com

        notmuch tag +calendar -- tag:new and mimetype:text/calendar
        notmuch tag +notification -- tag:new and from:notifications@

        notmuch tag +finance +financeInvoice -- tag:new and \( subject:invoice or body:invoice \)
        notmuch tag +finance +financeReceipt -- tag:new and \( subject:receipt or body:receipt \)
        notmuch tag +finance +financeOrder -- tag:new and \( subject:order or body:order or subject:bestell or body:bestell \)
        notmuch tag -financeInvoice -- tag:new and tag:financeReceipt
        notmuch tag -finance -financeOrder +notification -- tag:new and \( \( from:shipment-tracking@amazon.de and subject:'/^Arriving\stoday:/' \) or \( from:order-update@amazon.de and subject:'/^Delivered:/' \) \)

        notmuch tag +important -- Importance:high
        notmuch tag +unimportant -- Importance:low
        notmuch tag -new -- tag:sent
        notmuch tag -unread -inbox -new -- tag:new and tag:killed

        notmuch tag +release +games -new -- tag:new and subject:'/is\snow\savailable\son\sSteam!$/'
      ''
      (mkAfter ''
        notify() {
          tag=$1
          shift
          urgency=$1
          shift
          msg=$1
          shift
          count=$(notmuch count "$@")
          if [[ $count != 0 ]] ; then
            msg=$(printf "$msg" $count)
            dunstify -h string:x-dunst-stack-tag:notmuch"$tag" -a notmuch -u $urgency "$msg"
          fi
        }
        notify Important critical "%d new important email(s)" tag:new and tag:important
        notify Normal normal "%d new normal email(s)" tag:new and not tag:important and not tag:unimportant
        notify Unimportant low "%d new unimportant email(s)" tag:new and tag:unimportant
        notmuch tag +unread +inbox -new -- tag:new
        notmuch tag -inbox -- tag:dmz
        notmuch tag -inbox -- tag:inbox and folder:'/\/Archive\//'
      '')
    ];
    new.tags = [ "new" ];
    extraConfig = { index = { "header.Importance" = "Importance"; }; };
  };
  programs.mbsync.enable = true;
  services.mbsync = {
    enable = true;
    postExec = "${pkgs.notmuch}/bin/notmuch new";
  };
  programs.afew = {
    enable = true;
    extraConfig = ''
      [MailMover]
      folders = home/Archive home/Inbox home/Spam home/Folders/DMZ

      home/Archive = "not tag:archive":home/Inbox
      home/Spam = "not tag:spam":home/Inbox
      home/Folders/DMZ = "not tag:dmz":home/Inbox
      home/Inbox = "tag:archive":home/Archive "tag:spam":home/Spam "tag:dmz":home/Folders/DMZ "tag:killed":home/Trash
    '';
  };
  systemd.user.services.mbsync.Service.Environment =
    [ "NOTMUCH_CONFIG=/home/Myhlamaeus/.config/notmuch/default/config" ];
  programs.msmtp.enable = true;

  home.file.".mailcap" = {
    text = ''
      text/html;  ${pkgs.w3m}/bin/w3m -dump -o document_charset=%{charset} '%s'; nametemplate=%s.html; copiousoutput
    '';
  };

  home.file.".mailrc" = {
    text = mkDefault ''
      set sendmail="msmtp"
    '';
  };

  systemd.user.services.emacs.Service.Environment =
    [ "NOTMUCH_CONFIG=/home/Myhlamaeus/.config/notmuch/default/config" ];

  xdg.mimeApps = {
    associations.added = { "x-scheme-handler/mailto" = "emacs.desktop"; };

    defaultApplications = { "x-scheme-handler/mailto" = "emacs.desktop"; };
  };
}
