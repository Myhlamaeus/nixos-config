{ inputs, config, pkgs, lib, ... }:

let knownHosts = { };

in {
  programs.ssh = {
    enable = true;

    matchBlocks = {
      "github.com".user = "git";
      "gitlab.com".user = "git";

      rpi.user = "Myhlamaeus";
      sigma.user = "Myhlamaeus";
    };
  };
}
