{ config, pkgs, lib, ... }:

let
  cfg = config.programs.darcs;
  inherit (lib) concatStringsSep;

in {
  config = {
    home.packages = with pkgs; [ darcs ];

    home.file.".darcs/author".text = ''
      ${config.programs.git.userName} <${config.programs.git.userEmail}>
    '';
    home.file.".darcs/boring".text =
      concatStringsSep "\n" config.programs.git.ignores;
  };
}
