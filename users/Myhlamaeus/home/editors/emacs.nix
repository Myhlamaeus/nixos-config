{ config, lib, pkgs, ... }:

with lib;
let cfg = config.custom.editors;

in {
  options.custom.editors.emacs = {
    setup = mkOption { type = with types; lines; };

    spacemacs = {
      ref = mkOption {
        type = with types; str;
        default = "develop";
      };
      rev = mkOption {
        type = with types; str;
        default = "b86a074437503677d21e2172bd175b37edbdb029";
      };
    };
  };

  config = {
    custom.editors.env.bin.packages = with pkgs; [
      gnuplot
      unzip
      texlive.combined.scheme-full
      python3
      global
      cmake
      pandoc
    ];

    xdg.configFile."emacs/private" = {
      source = ./emacs;
      recursive = true;
    };

    xdg.configFile."emacs/private/user-init.el" = {
      text = let
        bin = (pkgs.symlinkJoin {
          name = "editor-env-bin";
          paths = cfg.env.bin.packages;
        }) + "/bin";
      in ''
        ${concatStringsSep "\n"
        (mapAttrsToList (k: v: ''(setenv "${k}" "${v}")'') cfg.env.vars)}
        (setenv "PATH" (concat "${bin}:" (getenv "PATH")))
        (add-to-list 'exec-path "${bin}" t)
        ${cfg.emacs.setup}
      '';
    };

    programs.emacs = {
      enable = true;

      package = (pkgs.emacs.override {
        # Use gtk3 instead of the default gtk2
        withGTK3 = true;
        withGTK2 = false;
      }).overrideAttrs (attrs: rec {
        # Use emacsclient in the .desktop file
        postInstall = (attrs.postInstall or "") + ''
          ${pkgs.gnused}/bin/sed -i 's/Exec=emacs/Exec=emacsclient -c -a emacs/' $out/share/applications/emacs.desktop
        '';
      });

      extraPackages = epkgs:
        with epkgs; [
          pdf-tools
          vterm
          xref
          emacsql
          emacsql-sqlite
        ];

      overrides = self: super: {
        pdf-tools = pkgs.nixpkgs-unstable.emacsPackages.pdf-tools;
      };
    };

    services.emacs = {
      enable = true;
      socketActivation.enable = true;
    };

    home.activation.spacemacs-setup = lib.hm.dag.entryAfter [ "writeBoundary" ]
      (pkgs.writeShellApplication {
        name = "spacemacs-setup";
        runtimeInputs = [ pkgs.git ];
        text = ''
          if ! [ -e ~/.config/emacs ] ; then
            # shellcheck disable=SC2086
            $DRY_RUN_CMD git \
              clone $VERBOSE_ARG \
              -b ${escapeShellArg cfg.emacs.spacemacs.ref} \
              https://github.com/syl20bnr/spacemacs \
              ~/.config/emacs
          fi
          if ! [ -e ~/.config/emacs/.git ] ; then
            temp=$(mktemp -d)
            mv ~/.config/emacs/private "$temp"
            # shellcheck disable=SC2086
            $DRY_RUN_CMD git \
              clone $VERBOSE_ARG \
              -b ${escapeShellArg cfg.emacs.spacemacs.ref} \
              https://github.com/syl20bnr/spacemacs \
              ~/.config/emacs
            rm -r ~/.config/emacs/private
            mv "$temp"/private ~/.config/emacs
            rm -r "$temp"
          fi
          if ! [ -L ~/.config/emacs/elfeed.score ] ; then
            rm -f ~/.config/emacs/elfeed.score
            # shellcheck disable=SC2086
            ln -s $VERBOSE_ARG /etc/nixos/users/Myhlamaeus/elfeed.score ~/.config/emacs/elfeed.score
            # shellcheck disable=SC2086
            $DRY_RUN_CMD git \
              clone $VERBOSE_ARG \
              -b ${escapeShellArg cfg.emacs.spacemacs.ref} \
              https://github.com/syl20bnr/spacemacs \
              ~/.config/emacs
          fi
          # shellcheck disable=SC2086
          $DRY_RUN_CMD git \
            --git-dir ~/.config/emacs/.git \
            --work-tree ~/.config/emacs \
            fetch $VERBOSE_ARG \
            origin \
            ${escapeShellArg cfg.emacs.spacemacs.ref}
          $DRY_RUN_CMD git \
            --git-dir ~/.config/emacs/.git \
            --work-tree ~/.config/emacs \
            update-ref \
            refs/heads/${escapeShellArg cfg.emacs.spacemacs.ref} \
            ${escapeShellArg cfg.emacs.spacemacs.rev}
          $DRY_RUN_CMD git \
            --git-dir ~/.config/emacs/.git \
            --work-tree ~/.config/emacs \
            checkout \
            ${escapeShellArg cfg.emacs.spacemacs.ref}
        '';
      } + "/bin/spacemacs-setup");

    home.activation.org-mode = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      if ! [ -L ~/org ] ; then
        $DRY_RUN_CMD ln -s $VERBOSE_ARG \
          ~/media/keybase/private/myhlamaeus/org \
          ~/org
      fi
    '';

    systemd.user.services.emacs.Service.Requires =
      mkForce "gpg-agent.service app.slice basic.target";
    systemd.user.services.emacs.Service.Environment =
      [ "SPACEMACSDIR=${config.home.sessionVariables.SPACEMACSDIR}" ];

    home.packages = with pkgs;
      [
        (makeDesktopItem rec {
          name = "org-protocol";
          desktopName = name;
          exec = "emacsclient %u";
          categories = [ "System" ];
          mimeTypes = [ "x-scheme-handler/org-protocol" ];
        })
      ];
    xdg.mimeApps = {
      defaultApplications = {
        "x-scheme-handler/org-protocol" = "org-protocol.desktop";
      };
    };

    home.sessionVariables = {
      SPACEMACSDIR = "/home/Myhlamaeus/.config/spacemacs";
    };
  };
}
