{ config, pkgs, ... }:

let npmPath = "${config.xdg.configHome}/npm";

in {
  # Doesn't work because bin/ is a symlink
  # custom.editors.env.bin.packages = with pkgs.nodePackages; [ eslint prettier typescript-language-server ];
  home.packages = with pkgs.nodePackages; [
    pkgs.nodejs
    eslint
    prettier
    typescript-language-server
  ];
  custom.editors.emacs.setup = ''
    (setenv "PATH" (concat "${npmPath}/bin:" (getenv "PATH")))
    (add-to-list 'exec-path "${npmPath}/bin" t)
  '';
  home.file.".npmrc".source =
    (pkgs.formats.toml { }).generate "home-npmrc" { prefix = npmPath; };
}
