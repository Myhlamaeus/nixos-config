{ inputs, config, lib, ... }:

let
  inherit (builtins) isAttrs;
  inherit (lib)
    mkOption mkEnableOption mkIf mkDefault types pipe filterAttrs mapAttrs
    substring;
  modulesType = types.listOf
    (types.oneOf [ (types.functionTo types.attrs) types.attrs types.path ]);
  cfg = config.nixos;
  buildSystem = extraModules: n:
    { modules, prefix, specialArgs, nixpkgs, baseModules, asNixosConfiguration
    , asPackage }:
    (import (nixpkgs.outPath + "/nixos/lib/default.nix") {
      inherit (nixpkgs) lib;
      # Implicit use of feature is noted in implementation.
      featureFlags.minimalModules = { };
    }).evalModules {
      inherit prefix;
      specialArgs = { inherit inputs; } // specialArgs;
      modules = baseModules ++ extraModules ++ [
        ({ config, options, ... }: {
          _module.args = { inherit baseModules modules; };
        })
      ] ++ cfg.sharedConfiguration.modules ++ modules ++ [{
        networking.hostName = mkDefault n;
        system.nixos.versionSuffix = ".${
            substring 0 8
            (nixpkgs.lastModifiedDate or nixpkgs.lastModified or "19700101")
          }.${nixpkgs.shortRev or "dirty"}";
        system.nixos.revision = mkIf (nixpkgs ? rev) nixpkgs.rev;
        # Let 'nixos-version --json' know about the Git revision
        # of this flake.
        system.configurationRevision =
          lib.mkIf (inputs.self ? rev) inputs.self.rev;
      }];
    };
  systemToString = system: if isAttrs system then system.system else system;

in {
  options.nixos = {
    sharedConfiguration = {
      modules = mkOption {
        type = modulesType;
        default = [ ];
      };
    };

    configurations = mkOption {
      type = types.attrsOf (types.submodule ({ config, ... }: {
        options = {
          modules = mkOption { type = modulesType; };

          prefix = mkOption {
            type = types.listOf types.anything;
            default = [ ];
          };

          specialArgs = mkOption {
            type = types.attrs;
            default = { };
          };

          nixpkgs = mkOption {
            type = types.attrs;
            default = inputs.nixpkgs;
          };
          baseModules = mkOption {
            type = modulesType;
            default = import
              (config.nixpkgs.outPath + "/nixos/modules/module-list.nix");
          };

          asNixosConfiguration = {
            enable = mkEnableOption "nixos as nixosConfiguration";
          };

          asPackage = {
            enable = mkEnableOption "nixos as package";

            configToPackage =
              mkOption { type = types.functionTo types.package; };
          };
        };

        config = {
          asNixosConfiguration.enable = mkDefault true;
          asPackage.enable = mkDefault false;
        };
      }));
      default = { };
    };
  };

  config = {
    flake.nixosConfigurations = pipe cfg.configurations [
      (filterAttrs (n: cfg: cfg.asNixosConfiguration.enable))
      (mapAttrs (buildSystem [ ]))
    ];

    perSystem = { system, ... }: {
      packages = pipe cfg.configurations [
        (filterAttrs (n: cfg: cfg.asPackage.enable))
        (mapAttrs (n: cfg: {
          inherit cfg;
          sys = buildSystem [{ nixpkgs.hostPlatform = system; }] n cfg;
        }))
        (filterAttrs (n:
          { sys, ... }:
          (systemToString sys.config.nixpkgs.hostPlatform) == system))
        (mapAttrs (n: { cfg, sys }: cfg.asPackage.configToPackage sys))
      ];
    };
  };
}
