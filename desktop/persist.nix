{ config, pkgs, lib, ... }:

{
  btrfsOptInState = {
    enable = true;

    btrfs = { device = "/dev/mapper/system"; };

    paths = [ /etc/nixos ];
  };

  home-manager.users.Myhlamaeus = {
    btrfsOptInState = {
      home.paths = [
        /.aws
        /.elfeed
        /.gnupg
        /.local/state/protonmail
        /.mozilla/firefox
        /.password-store
        /.ssh
        /.var/app/org.telegram.desktop
        /.wine-ebook-drm
        /.zotero
        /calibre-ebook-drm
        /org
      ];

      xdg.configPaths =
        [ /Element /calibre /chromium /emacs /email /keybase /protonmail ];

      xdg.dataPaths =
        [ /Steam /direnv /etesync-dav /flatpak /keybase /tor-browser /zsh ];
    };

    # zsh keeps on replacing the .zsh_history ln with a new file
    programs.zsh.history.path = "${
        toString config.btrfsOptInState.target.mountPoint
      }/home/Myhlamaeus/.local/share/zsh/history";
  };
}
