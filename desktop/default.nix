{
  imports = [
    (import ./disko.nix {
      rootDisk = "/dev/disk/by-id/wwn-0x5002538f414940cb";
      gamesDisk = "/dev/disk/by-id/wwn-0x5002538e4041f305";
    })
    ./persist.nix
    ./backups.nix
  ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "23.05"; # Did you read the comment?
  home-manager.users.Myhlamaeus.home.stateVersion = "23.05";
}
