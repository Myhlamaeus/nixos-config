{ config, lib, pkgs, ... }:

{
  # anbox
  networking.useDHCP = false;
  networking.interfaces.enp2s0.useDHCP = true;

  # encrypted boot
  boot.loader = {
    efi.canTouchEfiVariables = true;
    efi.efiSysMountPoint = "/boot/efi";
    grub = {
      enable = true;
      version = 2;
      device = "nodev";
      efiSupport = true;
      enableCryptodisk = true;
    };
  };
  boot.supportedFilesystems = [ "btrfs" ];

  # Video drivers
  services.xserver = {
    screenSection = ''
      Option "metamodes" "2560x1440_144 +0+0 {ForceCompositionPipeline=On, ForceFullCompositionPipeline=On}"
    '';
  };
  hardware.opengl = { extraPackages = with pkgs; [ libvdpau-va-gl ]; };
  virtualisation.docker.enableNvidia = true;

  boot.initrd.availableKernelModules =
    [ "xhci_pci" "ahci" "uas" "usb_storage" "usbhid" "sd_mod" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/var/log".neededForBoot = true;

  swapDevices = [{ device = "/dev/mapper/swap"; }];

  nix.settings.max-jobs = lib.mkDefault 8;
  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  # High-DPI console
  console.font =
    lib.mkDefault "${pkgs.terminus_font}/share/consolefonts/ter-u28n.psf.gz";

  hardware.bluetooth.enable = true;
  hardware.bluetooth.settings = {
    General = { Enable = "Source,Sink,Media,Socket"; };
  };
}
