{
  services.btrfsBackups.backups = {
    default = {
      enable = true;
      environmentFile = "/etc/nixos/secrets/btrfsBackups";
      mountPoint = /btrfs;
      subvolumes = [ "ebooks" ];
    };
  };
}
