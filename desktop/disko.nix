{ rootDisk ? "/dev/vdb", gamesDisk ? "/dev/vdc", ... }:

{
  disko.devices = {
    disk = {
      root = {
        type = "disk";
        device = rootDisk;
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              size = "512M";
              type = "EF00";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot/efi";
                mountOptions = [ "defaults" ];
              };
            };
            boot = {
              size = "1G";
              content = {
                type = "luks";
                name = "boot";
                extraOpenArgs = [ "--allow-discards" ];
                # if you want to use the key for interactive login be sure there is no trailing newline
                # for example use `echo -n "password" > /tmp/secret.key`
                passwordFile = "/tmp/secret.key"; # Interactive
                # settings.keyFile = "/tmp/secret.key";
                # additionalKeyFiles = [ "/tmp/additionalSecret.key" ];
                content = {
                  type = "filesystem";
                  format = "ext2";
                  mountpoint = "/boot";
                };
              };
            };
            swap = {
              size = "35G";
              content = {
                type = "luks";
                name = "swap";
                extraOpenArgs = [ "--allow-discards" ];
                # if you want to use the key for interactive login be sure there is no trailing newline
                # for example use `echo -n "password" > /tmp/secret.key`
                passwordFile = "/tmp/secret.key"; # Interactive
                # settings.keyFile = "/tmp/secret.key";
                # additionalKeyFiles = [ "/tmp/additionalSecret.key" ];
                content = {
                  type = "swap";
                  randomEncryption = false;
                  resumeDevice = true;
                };
              };
            };
            system = {
              size = "100%";
              content = {
                type = "luks";
                name = "system";
                extraOpenArgs = [ "--allow-discards" ];
                # if you want to use the key for interactive login be sure there is no trailing newline
                # for example use `echo -n "password" > /tmp/secret.key`
                passwordFile = "/tmp/secret.key"; # Interactive
                # settings.keyFile = "/tmp/secret.key";
                # additionalKeyFiles = [ "/tmp/additionalSecret.key" ];
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ];
                  subvolumes = {
                    "/root" = {
                      mountpoint = "/";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/nix" = {
                      mountpoint = "/nix";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/persist" = {
                      mountpoint = "/persist";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/log" = {
                      mountpoint = "/var/log";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                    "/code" = {
                      mountpoint = "/home/Myhlamaeus/.ghq";
                      mountOptions = [ "compress-force=zstd" "noatime" ];
                    };
                    "/email" = {
                      mountpoint = "/home/Myhlamaeus/Maildir";
                      mountOptions = [ "compress-force=zstd" "noatime" ];
                    };
                    "/ebooks" = {
                      mountpoint = "/home/Myhlamaeus/Calibre Library";
                      mountOptions = [ "compress=zstd" "noatime" ];
                    };
                  };
                };
              };
            };
          };
        };
      };
      games = {
        type = "disk";
        device = gamesDisk;
        content = {
          type = "gpt";
          partitions = {
            games = {
              size = "100%";
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/home/Myhlamaeus/media/games";
              };
            };
          };
        };
      };
    };
  };
}
