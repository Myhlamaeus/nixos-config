{ config, pkgs, lib, ... }:

{
  imports = [
    (import ./disko.nix {
      rootDisk = "/dev/disk/by-id/nvme-eui.002538b631b44ec0";
    })
    ./backups.nix
    ./containers.nix
    ./gateway
    ./persist.nix
    ./rpi-compat
    ./secrets.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.firewall = { allowedTCPPorts = [ 80 443 ]; };
  containers.gateway.config.security.acme = {
    acceptTerms = true;
    defaults.email = "admin@maublew.name";
  };

  # required to let containers access internet
  networking.nat = {
    enable = true;
    internalInterfaces = [ "ve-+" ];
    externalInterface = "enp1s0";
  };

  gateway = {
    enable = true;
    root = "maublew.name";
    targetHost = "10.233.0.1";
  };

  # ssh setup
  boot.initrd.availableKernelModules = [ "igc" ];
  boot.kernelParams = [ "ip=dhcp" ];
  # @TODO Move to server once rpi is adjusted
  boot.initrd.network.enable = true;
  boot.initrd.network.ssh = {
    shell = "/bin/cryptsetup-askpass";
    hostKeys = [ "/etc/secrets/initrd/ssh_host_ed25519_key" ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "23.05"; # Did you read the comment?
  home-manager.users.Myhlamaeus.home.stateVersion = "23.05";
}
