{ config, pkgs, lib, ... }:

{
  btrfsOptInState = {
    enable = true;

    btrfs = { device = "/dev/mapper/system"; };

    paths = [
      /etc/nixos
      /etc/shadow
      /etc/ssh/ssh_host_ed25519_key
      /etc/ssh/ssh_host_ed25519_key.pub
      /etc/ssh/ssh_host_rsa_key
      /etc/ssh/ssh_host_rsa_key.pub
    ];
  };

  home-manager.users.Myhlamaeus = {
    btrfsOptInState = {
      home.paths = [ /.ssh ];

      xdg.configPaths = [ ];

      xdg.dataPaths = [ /direnv /flatpak /zsh ];
    };

    # zsh keeps on replacing the .zsh_history ln with a new file
    programs.zsh.history.path =
      "${config.btrfsOptInState.target.mountPoint}/home/Myhlamaeus/.local/share/zsh/history";
  };
}
