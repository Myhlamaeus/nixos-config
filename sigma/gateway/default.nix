{ options, config, lib, ... }:

let
  inherit (lib) mkOption mkIf types;
  cfg = config.gateway;
in {
  options.gateway = options.services.nginx.gateway;

  config = mkIf cfg.enable {
    containers.gateway = {
      privateNetwork = false;

      forwardPorts = [
        {
          hostPort = 80;
          containerPort =
            config.containers.gateway.config.services.nginx.defaultHTTPListenPort;
        }
        {
          hostPort = 443;
          containerPort =
            config.containers.gateway.config.services.nginx.defaultSSLListenPort;
        }
      ];

      autoStart = true;

      config = {
        system.stateVersion = "23.05";

        imports = [ ./secrets.nix ];

        services.nginx = {
          enable = true;

          recommendedTlsSettings = true;
          recommendedZstdSettings = true;
          recommendedOptimisation = true;
          recommendedGzipSettings = true;
          recommendedProxySettings = true;
          recommendedBrotliSettings = true;

          gateway = cfg;

          virtualHosts = {
            ${config.networking.domain} = {
              enableACME = true;
              forceSSL = true;
            };

            "${config.networking.hostName}.${config.networking.domain}" = {
              enableACME = true;
              forceSSL = true;
            };
          };
        };
      };
    };
  };
}
