{ inputs, config, pkgs, lib, ... }:

let inherit (lib) mkOption mkDefault types mkMerge mkIf pipe mapAttrs;
in {
  options.containers = mkOption {
    type = types.attrsOf (types.submodule ({ name, ... }@args:
      let ageId = /run/host-agenix/containers + "/${name}";
      in {
        config = {
          privateNetwork = mkDefault true;
          hostAddress = mkIf args.config.privateNetwork "10.233.0.1";

          bindMounts.age = {
            hostPath = config.age.secrets."containers/${name}".path;
            mountPoint = toString ageId;
            isReadOnly = true;
          };

          config = {
            imports = [
              ../shared/secrets.nix
              inputs.agenix.nixosModules.default
              inputs.self.nixosModules.nginxGateway
              inputs.self.nixosModules.postgresqlInit
            ];

            environment.systemPackages = with pkgs; [ rxvt_unicode.terminfo ];

            nix = { inherit (config.nix) package extraOptions registry; };

            age.custom.secrets."users/Myhlamaeus/encryptedPassword".enable =
              false;

            age.identityPaths = [ (toString ageId) ];
          };
        };
      }));
  };
}
