{ inputs, config, lib, ... }:

let
  cfg = { stateDir = /var/lib/rpi-compat; };
  internalStateDir = /var/lib;
  postgresqlDir = internalStateDir + "/postgresql";
  inherit (lib) mkForce;
  conbeeDev =
    /dev/serial/by-id/usb-dresden_elektronik_ingenieurtechnik_GmbH_ConBee_II_DE2230473-if00;

in {
  systemd.tmpfiles.rules = [ "d ${toString cfg.stateDir} 1777 root root -" ];

  containers.rpi-compat = {
    localAddress = "10.233.2.2";

    forwardPorts = [{
      hostPort = 8081;
      containerPort =
        config.containers.rpi-compat.config.services.nginx.defaultHTTPListenPort;
    }];

    bindMounts.rpi-compat = {
      hostPath = toString cfg.stateDir;
      mountPoint = toString /var/lib;
      isReadOnly = false;
    };

    allowedDevices = [{
      node = toString conbeeDev;
      modifier = "rwm";
    }];

    bindMounts.dev = {
      hostPath = toString conbeeDev;
      mountPoint = toString conbeeDev;
      isReadOnly = false;
    };

    autoStart = true;

    config = {
      system.stateVersion = "23.05";

      networking.domain = "maurice-dreyer.name";
      networking.hostName = "rpi";
      networking.firewall = {
        enable = true;
        allowedTCPPorts = [ 80 ];
      };

      imports = [
        ./secrets.nix
        ./etebase.nix
        ./gitea.nix
        ./home-assistant.nix
        ./matrix.nix
        ({ config, pkgs, ... }: {
          services.postgresql.dataDir = toString (postgresqlDir
            + "/${config.services.postgresql.package.psqlSchema}");
        })
      ];

      services.nginx = {
        enable = true;

        recommendedGzipSettings = true;
        recommendedProxySettings = true;
      };

      services.gitea.package =
        inputs.nixpkgs-unstable.legacyPackages.${config.nixpkgs.hostPlatform.system}.gitea;

      custom.etebase.enable = true;
      custom.gitea.enable = true;
      custom.home-assistant.enable = true;
      custom.matrix-synapse.enable = true;
    };
  };

  gateway.services = let inherit (lib) pipe nameValuePair listToAttrs;
  in {
    rpi-compat = {
      hostName = "maurice-dreyer.name";
      targetPort = 8081;
    };
  } // pipe [ "element" "etebase" "git" "matrix" "home" ] [
    (map (n:
      nameValuePair "${n}-rpi-compat" {
        hostName = "${n}.maurice-dreyer.name";
        targetPort = 8081;
      }))
    listToAttrs
  ];
}
