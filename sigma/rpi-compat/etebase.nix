{ config, pkgs, lib, ... }:

with lib;

let cfg = config.custom.etebase;

in {
  options.custom.etebase = {
    enable = mkEnableOption "custom.etebase";

    serverName = mkOption {
      type = with types; str;
      default = config.networking.domain;
    };

    etebaseHostname = mkOption {
      type = with types; str;
      default = "etebase.${cfg.serverName}";
    };

    etebasePort = mkOption {
      type = with types; port;
      default = 8001;
    };
  };

  config = mkIf cfg.enable {
    age.secrets."etebase/server".owner = "etebase-server";

    services.etebase-server = {
      enable = true;
      port = cfg.etebasePort;
      settings = {
        global = { secret_file = config.age.secrets."etebase/server".path; };
        allowed_hosts = { allowed_host1 = cfg.etebaseHostname; };
      };
    };

    services.nginx.virtualHosts.${cfg.etebaseHostname} = {
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString cfg.etebasePort}";
        proxyWebsockets = true;
      };
    };
  };
}
