{ config, pkgs, lib, ... }:

with lib;

let cfg = config.custom.gitea;

in {
  options.custom.gitea = {
    enable = mkEnableOption "custom.gitea";

    serverName = mkOption {
      type = with types; str;
      default = config.networking.domain;
    };

    gitHostname = mkOption {
      type = with types; str;
      default = "git.${cfg.serverName}";
    };
  };

  config = mkIf cfg.enable {
    services.postgresql = {
      enable = true;

      ensureUsers = [{ name = config.services.gitea.database.user; }];
    };

    services.gitea = {
      enable = true;
      database = {
        type = "postgres";
        socket = "/run/postgresql";
      };
      lfs.enable = true;
      settings.server = {
        PROTOCOL = "http+unix";
        ROOT_URL = "https://${cfg.gitHostname}";
      };
      settings.service = { DISABLE_REGISTRATION = true; };
      settings.session = { COOKIE_SECURE = true; };
    };

    services.nginx.virtualHosts.${cfg.gitHostname} = {
      locations."/" = {
        proxyPass = "http://unix:/run/gitea/gitea.sock:/";
        proxyWebsockets = true;
      };
    };
  };
}
