{ config, pkgs, lib, ... }:

with lib;

let cfg = config.custom.home-assistant;

in {
  options.custom.home-assistant = {
    enable = mkEnableOption "custom.home-assistant";

    serverName = mkOption {
      type = with types; str;
      default = config.networking.domain;
    };

    homeAssistantHostname = mkOption {
      type = with types; str;
      default = "home.${cfg.serverName}";
    };

    homeAssistantPort = mkOption {
      type = with types; port;
      default = 8123;
    };
  };

  config = mkIf cfg.enable {
    services.home-assistant = {
      enable = true;

      package = (pkgs.home-assistant.overrideAttrs (oldAttrs: {
        doCheck = false;
        doInstallCheck = false;
      })).override { extraComponents = [ "otp" ]; };

      openFirewall = true;

      config = let
        secret = name: "!secret ${name}";
        mkCondition = condition: cfg: { inherit condition; } // cfg;
        mkPlatform = platform: cfg: { inherit platform; } // cfg;
        mkService = service: cfg: { inherit service; } // cfg;
        _time = "time";
        _timeDate = "time_date";
        _state = "state";
        _sun = "sun";
        _home = "home";
        _notHome = "not_home";
        _persons = { maurice = "maurice"; };
        _scenes = {
          wakeUp = "wake_up";
          bedroom = {
            day = "day_bedroom";
            evening = "evening_bedroom";
            night = "night_bedroom";
            away = "away_bedroom";
          };
          hallway = {
            on = "hallway_on";
            off = "hallway_off";
          };
        };
        _on = "on";
        _off = "off";
        _rooms = {
          bedroom = "bedroom_lights";
          hallway = "hallway_lights";
          kitchen = "kitchen_lights";
        };
      in {
        homeassistant = {
          name = "Home";
          latitude = secret "latitude";
          longitude = secret "longitude";
          elevation = secret "elevation";
          unit_system = "metric";
          time_zone = "UTC";
          temperature_unit = "C";
          external_url = "https://${cfg.homeAssistantHostname}";
          internal_url = "https://${cfg.homeAssistantHostname}";
        };

        default_config = { };

        # owntracks = {
        #   mqtt_topic = "owntracks/#";
        #   secret = secret "owntracks_secret";
        # };

        http = {
          use_x_forwarded_for = true;
          trusted_proxies = [ "127.0.0.1" ];
        };

        zha = {
          database_path = "/var/lib/hass/zigbee.db";
          zigpy_config = { ota = { ikea_provider = true; }; };
        };

        zeroconf = { };

        sensor = [
          (mkPlatform _timeDate { display_options = [ "time" ]; })

          # missing input
          # (mkPlatform "coronavirus"{
          #   # country = "de";
          # })
        ];

        scene = let
          mkLight = name: state: o: {
            "light.${name}" = { inherit state; } // o;
          };
          light = {
            on = name:
              { transition ? 600, brightness ? 1, temperature ? 4000 }:
              mkLight name _on {
                inherit transition;
                brightness_pct = brightness * 100;
                kelvin = temperature;
              };
            off = name:
              { transition ? 600 }:
              mkLight name _off { inherit transition; };
            toggle = name:
              { transition ? 600, brightness ? 1, temperature ? 4000 }:
              mkLight name _toggle {
                inherit transition;
                brightness_pct = brightness * 100;
                kelvin = temperature;
              };
          };
          scene = name: entities: { inherit name entities; };
        in [
          (scene "Wake-up" (light.on _rooms.bedroom { temperature = 4000; }))
          (scene "Away bedroom" (light.off _rooms.bedroom { transition = 0; }))
          (scene "Day bedroom" (light.off _rooms.bedroom { }))
          (scene "Evening bedroom" (light.on _rooms.bedroom {
            brightness = 0.8;
            temperature = 3000;
          }))
          (scene "Night bedroom" (light.on _rooms.bedroom {
            brightness = 0.4;
            temperature = 2000;
          }))
          (scene "Hallway On" (light.on _rooms.hallway {
            brightness = 0.8;
            temperature = 3000;
          }))
          (scene "Hallway Off" (light.off _rooms.hallway { }))
        ];

        automation = let
          once = {
            time = at: mkPlatform _time { inherit at; };
            state = entity_id: to: mkPlatform _state { inherit entity_id to; };
            home = name: once.state "person.${name}" _home;
            notHome = name: once.state "person.${name}" _notHome;
            sun = event: offset: mkPlatform _sun { inherit event offset; };
            zhaCommand = device_id: cluster_id: command: {
              platform = "event";
              event_type = "zha_event";
              event_data = { inherit device_id cluster_id command; };
            };
          };
          if' = {
            state = entity_id: state:
              mkCondition _state { inherit entity_id state; };
            home = name: if'.state "person.${name}" _home;
            timeAfter = after: mkCondition _time { inherit after; };
            timeBetween = after: before:
              mkCondition _time { inherit after before; };
          };
          act = {
            scene = name:
              mkService "scene.turn_on" { entity_id = "scene.${name}"; };
          };
          auto = {
            actOnceIf = alias: trigger: condition: action: {
              inherit alias trigger condition action;
            };
            actOnce = alias: trigger: action:
              auto.actOnceIf alias trigger [ ] action;
            sceneOnceIf = alias: trigger: name: condition:
              auto.actOnceIf alias trigger condition (act.scene name);
            sceneOnce = alias: trigger: name:
              auto.sceneOnceIf alias trigger name [ ];
            sceneAtIf = alias: at: auto.sceneOnceIf alias (once.time at);
            sceneAt = alias: at: name: auto.sceneAtIf alias at name [ ];
            scenesZheOnOff = alias: id: scene: [
              (auto.sceneOnce "${alias} On" [ (once.zhaCommand id 6 "on") ]
                scene.on)
              (auto.sceneOnce "${alias} Off" [ (once.zhaCommand id 6 "off") ]
                scene.off)
            ];
          };
        in [
          (auto.sceneAtIf "Wake-up" "02:50:00" _scenes.wakeUp
            [ (if'.home _persons.maurice) ])
          (auto.sceneAt "Day Bedroom" "08:00:00" _scenes.bedroom.day)

          (auto.actOnceIf "Evening Bedroom" [
            (once.sun "sunset" "-01:00:00")
            (once.home _persons.maurice)
          ] [
            (if'.timeBetween "16:00:00" "18:00:00")
            (if'.home _persons.maurice)
          ] [ (act.scene _scenes.bedroom.evening) ])

          (auto.actOnceIf "Night Bedroom" [
            (once.time "18:00:00")
            (once.home _persons.maurice)
          ] [ (if'.timeAfter "18:00:00") (if'.home _persons.maurice) ]
            [ (act.scene _scenes.bedroom.night) ])

          (auto.actOnce "Away Bedroom" [ (once.notHome _persons.maurice) ]
            [ (act.scene _scenes.bedroom.away) ])
        ] ++ (auto.scenesZheOnOff "Hallway" "311fbde37436c9fe42501a9d2b878ad7"
          _scenes.hallway);
      };
      # configWritable = true; # doesn't work atm
    };

    services.nginx.virtualHosts.${cfg.homeAssistantHostname} = {
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString cfg.homeAssistantPort}";
        proxyWebsockets = true;
      };
    };
  };
}
