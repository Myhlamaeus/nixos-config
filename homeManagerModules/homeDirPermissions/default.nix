{ config, pkgs, lib, ... }:

let
  inherit (lib)
    mkIf mkEnableOption mkOption types pipe mapAttrsToList concatMapStringsSep;
  inherit (lib.hm) dag;
  cfg = config.services.homeDirPermissions;

in {
  options.services.homeDirPermissions = {
    enable = mkEnableOption "homeDirPermissions";

    paths = mkOption {
      type = types.attrsOf (types.submodule {
        options = { exclude = mkOption { type = with types; listOf str; }; };
      });
    };
  };

  config = mkIf cfg.enable {
    home.activation.home-dir-permissions = pipe cfg.paths [
      (mapAttrsToList (base: v: v // { inherit base; }))
      (concatMapStringsSep "\n" ({ base, exclude }: ''
        $DRY_RUN_CMD find ${base} \
          ${
            concatMapStringsSep "\n" (p: "-path ${base}/${p} -prune -o \\")
            exclude
          }
          -type d \
          -exec setfacl -dm "o::000" "{}" + \
          -exec setfacl -dm "g::000" "{}" + \
          -exec chmod go-rwx "{}" +
        $DRY_RUN_CMD find ${base} \
          ${
            concatMapStringsSep "\n" (p: "-path ${base}/${p} -prune -o \\")
            exclude
          }
          -type f \
          -exec chmod go-rwx "{}" +
      ''))
      (dag.entryAfter [ "writeBoundary" ])
    ];
  };
}
