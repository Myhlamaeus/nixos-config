{ inputs, pkgs, lib, ... }:

{
  imports = [
    ./cachix.nix
    ./ssh.nix
    ./backups.nix
    ./secrets.nix
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.default
    inputs.self.nixosModules.addInputsToRegistry
    inputs.self.nixosModules.btrfsBackups
    inputs.self.nixosModules.btrfsOptInState
    inputs.self.nixosModules.fontOverrides
    inputs.self.nixosModules.nginxGateway
    inputs.self.nixosModules.postgresqlInit
    inputs.home-manager.nixosModules.home-manager
    inputs.funkwhale.nixosModules.default
  ];

  home-manager.sharedModules = [
    inputs.felschr-nixos.homeManagerModules.git
    inputs.self.homeManagerModules.firefoxAutoProfile
    inputs.self.homeManagerModules.homeDirPermissions
    inputs.self.homeManagerModules.tridactyl
  ];

  btrfsOptInState.integrations.home-manager.enable = true;

  nix.package = pkgs.nixUnstable;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';

  nixpkgs.overlays = [
    inputs.funkwhale.overlays.default
    (import ./extra-packages.nix)

    (self: super: {
      veloren = inputs.veloren.packages.${self.system};
      gitignore = inputs.gitignore;
      cheatPackages = { community = inputs.cheatsheets; };
    })

    (self: super:
      let
        # dunno how to set allowUnfree with nixpkgs-unstable.legacyPackages.x86_64-linux
        unstable = import inputs.nixpkgs-unstable.outPath {
          inherit (self) system;
          config = { allowUnfree = true; };
        };
      in {
        nixpkgs-unstable = unstable;
        inherit (unstable)
          dwarf-fortress-packages emacs notmuch openhantek openmw
          tor-browser-bundle-bin zsh-completions steam protonmail-bridge
          osu-lazer zotero;
        inherit (unstable.gitAndTools) git-bug;
      })

    (self: super: {
      teensy-loader-cli = super.teensy-loader-cli.overrideAttrs (attrs: rec {
        postInstall = (attrs.postInstall or "") + ''
          mkdir -p $out/lib/udev/rules.d
          cp ${self.teensy-udev-rules} $out/lib/udev/rules.d/49-teensy.rules
        '';
      });
    })
  ];

  home-manager.extraSpecialArgs = { inherit inputs; };

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "en_GB.UTF-8";
    extraLocaleSettings = {
      LC_TIME = "en_DK.UTF-8";
      LC_MONETARY = "en_IE.UTF-8";
    };
    supportedLocales = [ "all" ];
  };
}
