{ config, pkgs, lib, ... }:

let
  inherit (lib) escapeShellArg mkIf;
  inherit (pkgs) writeShellScript rage rclone zstd;
  ageKey = "age182395wkl8y0fl5cwlpkcfhvart90r6vvg0lg69stnhp9hdcfwy4slryf66";
  target =
    ":b2:maublew-backups/${config.networking.domain}/${config.networking.hostName}";

in {
  services.btrfsBackups = {
    defaultPostprocessCommand = writeShellScript "defaultEncryptCommand" ''
      set -euo pipefail
      ${zstd}/bin/zstd -c8 "$1" | ${rage}/bin/rage -r ${escapeShellArg ageKey}
    '';

    defaultUploadCommand = writeShellScript "defaultUploadCommand" ''
      set -euo pipefail
      volume=$1
      time=$2
      ${rclone}/bin/rclone rcat ${escapeShellArg target}/"$volume/$time.zst.age"
    '';

    backups = {
      persist = mkIf config.btrfsOptInState.enable {
        environmentFile = "/etc/nixos/secrets/btrfsBackups";
        mountPoint = config.btrfsOptInState.btrfs.mountPoint;
        subvolumes = [ config.btrfsOptInState.target.volume ];
      };
    };
  };
}
