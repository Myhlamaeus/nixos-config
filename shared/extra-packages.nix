self: super: {
  teensy-udev-rules = builtins.fetchurl {
    url = "https://www.pjrc.com/teensy/49-teensy.rules";
    sha256 = "1qbl1f40jc0dg3z1lag2bk2b0nv54n9z3bdpwmdz09m50a0nskbv";
  };

  proton-ge-custom = let v = "GE-Proton7-9";
  in self.stdenv.mkDerivation rec {
    pname = "proton-ge-custom";
    version = "7.9";
    src = builtins.fetchurl {
      name = "${pname}-${version}-source";
      url =
        "https://github.com/GloriousEggroll/${pname}/releases/download/${v}/${v}.tar.gz";
      sha256 = "1c0axvbh16wkd3w8dcpvbjh0bl9pqgyhr0d17n23wabswgdsxcqs";
    };
    unpackCmd = ''
      mkdir out
      tar -xzf $curSrc -C out
    '';
    installPhase = ''
      cp -r ${v} $out
    '';
  };
}
