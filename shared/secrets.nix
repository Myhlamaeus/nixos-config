{ config, lib, ... }:

let
  inherit (builtins) readFile fromJSON isString;
  inherit (lib)
    pipe mapAttrs filterAttrs flip mkDefault mkMerge mkIf mkEnableOption
    mkOption types;
  cfg = config.age.custom;
  normalise = v:
    if isString v then {
      enable = mkDefault true;
      passPath = v;
    } else
      { enable = mkDefault true; } // v;

in {
  options.age.custom = {
    root = mkOption {
      type = types.nullOr types.path;
      default = null;
    };

    secretFiles = mkOption {
      type = types.listOf types.path;
      default = [ ];
    };

    secrets = mkOption {
      type = types.attrsOf (types.submodule {
        options = {
          enable = mkEnableOption "secret";

          passPath = mkOption { type = types.str; };
        };
      });
      default = { };
    };
  };

  config = mkIf (cfg.root != null) {
    age.custom.secretFiles = [ ./secrets.json ];
    age.custom.secrets = pipe cfg.secretFiles [
      (map (flip pipe [ readFile fromJSON (mapAttrs (n: normalise)) ]))
      mkMerge
    ];

    age.secrets = pipe cfg.secrets [
      (filterAttrs (n: v: v.enable))
      (mapAttrs (n: v: { file = cfg.root + "/secrets/${n}.age"; }))
    ];
  };
}
